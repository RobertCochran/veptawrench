package com.sorafirestorm.veptawrench;

import net.minecraft.item.Item;
import net.minecraft.creativetab.CreativeTabs;

import cpw.mods.fml.common.registry.GameRegistry;

public final class HandleItem extends Item
{
	public static Item handleItem;

	public static void createHandleItem()
	{
		HandleItem handleItem = new HandleItem();

		handleItem.setMaxStackSize(1);
		handleItem.setCreativeTab(CreativeTabs.tabMisc);
		handleItem.setUnlocalizedName("veptawrench-handle");
		handleItem.setTextureName("veptawrench:handle");

		GameRegistry.registerItem(handleItem, "veptawrench-handle");
	}
}
