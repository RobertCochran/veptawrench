package com.sorafirestorm.veptawrench;

import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.creativetab.CreativeTabs;

import cpw.mods.fml.common.registry.GameRegistry;

import com.sorafirestorm.veptawrench.HandleItem;
import com.sorafirestorm.veptawrench.CoreItem;
import com.sorafirestorm.veptawrench.HeadItem;

public final class WrenchItem extends Item
{
	public static Item wrenchItem;

	public static void createWrenchItem()
	{
		WrenchItem wrenchItem = new WrenchItem();

		wrenchItem.setMaxStackSize(1);
		wrenchItem.setCreativeTab(CreativeTabs.tabMisc);
		wrenchItem.setUnlocalizedName("veptawrench");
		wrenchItem.setTextureName("veptawrench:wrench");

		GameRegistry.registerItem(wrenchItem, "veptawrench");
	}

	public static void createWrenchRecipe()
	{
		ItemStack wrenchItem = new ItemStack(new WrenchItem());
		ItemStack handleItem = new ItemStack(new HandleItem());
		ItemStack coreItem = new ItemStack(new CoreItem());
		ItemStack headItem = new ItemStack(new HeadItem());

		GameRegistry.addRecipe(wrenchItem, "  x", " y ", "z  ",
			'x', headItem, 'y', coreItem, 'z', handleItem);
	}
}
