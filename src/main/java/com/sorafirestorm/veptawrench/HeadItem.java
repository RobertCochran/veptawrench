package com.sorafirestorm.veptawrench;

import net.minecraft.item.Item;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.creativetab.CreativeTabs;

import cpw.mods.fml.common.registry.GameRegistry;

public final class HeadItem extends Item
{
	public static Item headItem;

	public static void createHeadItem()
	{
		HeadItem headItem = new HeadItem();

		headItem.setMaxStackSize(1);
		headItem.setCreativeTab(CreativeTabs.tabMisc);
		headItem.setUnlocalizedName("veptawrench-head");
		headItem.setTextureName("veptawrench:head");

		GameRegistry.registerItem(headItem, "veptawrench-head");
	}

	/*public static void createHeadRecipe()
	{
		ItemStack ironBlock = new ItemStack("minecraft:iron_block");
		ItemStack ironIngot = new ItemStack(Item.ingotIron);

		GameRegistry.addRecipe(new ItemStack(new HeadItem()), "   ",
			"x x", "xyx", 'x', ironBlock, 'y', ironIngot);
			}*/
}
