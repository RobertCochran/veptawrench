package com.sorafirestorm.veptawrench;

import net.minecraft.creativetab.CreativeTabs;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.registry.GameRegistry;

import com.sorafirestorm.veptawrench.WrenchItem;

@Mod(modid = Veptawrench.MODID,
	name = Veptawrench.MODNAME, version = Veptawrench.VERSION)
public class Veptawrench {
	public static final String MODID = "veptawrench";
	public static final String MODNAME = "VeptaWrench";
	public static final String VERSION = "0.1";

	@Instance
	public static Veptawrench instance = new Veptawrench();


	@SidedProxy(clientSide = "com.sorafirestorm.veptawrench.ClientProxy", serverSide = "com.sorafirestorm.veptawrench.ServerProxy")
	public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		System.out.println("VeptaWrench preInit() started");

		WrenchItem.createWrenchItem();
		HandleItem.createHandleItem();
		CoreItem.createCoreItem();
		HeadItem.createHeadItem();

		this.proxy.preInit(e);
	}

	@EventHandler
	public void Init(FMLInitializationEvent e) {
		System.out.println("VeptaWrench Init() started");

		WrenchItem.createWrenchRecipe();

		this.proxy.init(e);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		System.out.println("VeptaWrench postInit() started");
		this.proxy.postInit(e);
	}
}
