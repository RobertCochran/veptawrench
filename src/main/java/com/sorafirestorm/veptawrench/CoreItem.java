package com.sorafirestorm.veptawrench;

import net.minecraft.item.Item;
import net.minecraft.creativetab.CreativeTabs;

import cpw.mods.fml.common.registry.GameRegistry;

public final class CoreItem extends Item
{
	public static Item coreItem;

	public static void createCoreItem()
	{
		CoreItem coreItem = new CoreItem();

		coreItem.setMaxStackSize(1);
		coreItem.setCreativeTab(CreativeTabs.tabMisc);
		coreItem.setUnlocalizedName("veptawrench-core");
		coreItem.setTextureName("veptawrench:core");

		GameRegistry.registerItem(coreItem, "veptawrench-core");
	}
}
